# Git Workflow

## flow
bv van een git situatie: 

	-> Master 
		-> Develop 
			-> Feature1
		-> Develop 
			-> Feature2
		<- Develop
	<- Master		
		-> Develop
			-> Feature3
		<- Develop
	<- Master

**Master branch** (productie branch: 100% WERKENDE CODE)

**Develop branch** (ontwikkel branch) 
-> vanuit hier maak je features (lokaal/ geen remote tenzij je met meer op de zelfde feature werkt).

**Feature branches** bv. Layout, Bugs, ...

**Bugs branch** als feature branch merge met de develop en er treed een conflict op. 

## Commands

**git checkout -b feature_1**  : maak een nieuwe branch ‘feature_1’.

**git checkout develop** : ga naar develop branch

**git add .** : voeg alle files toe.

**git status :** status van files 

**git commit -m ‘[FEATURE] – code step 1 (name)’** :
		[TASK]
		[BUG]

**git merge feature_1 –no-ff** : merge feature_1 in je huidige branch

**git push origin develop** : push develop naar de remote

