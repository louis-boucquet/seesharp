# CSRF
cross site request forgery

## html
```cs
// Also creates an anti forgery token
<form asp-action="Create">
```

## Controller (2 opties)

### per controller
All onveilige HTTP methodes (delete, post, put, patch) zijn beschermt. veilig methodes negeren de token validatie.
```cs
[AutoValidateAntiforgeryToken]
public class TicketRequestsController : Controller
```


### per methode
validate de ontvangen token wanneer er een POST HTTP request komt.
```cs
[ValidateAntiForgeryToken]
public IActionResult Create(TicketRequest ticketRequest)

```