# Rest API

A REST api also use the mvc framework with controller classes. The difference here is that we return json (or XML) instead of a view (which contains html).

## CRUD

Here's a table of the standard mapping from an http verd to a C# method.

| Requests (HTTP verb) | Method in Controller class|
| :- | :- |
| Get | Read |
| Post | Create |
| Put/Patch | Modify |
| Delete | Delete |

### URI Design

A controller contains a collection of data elements.

A `GET` request generaly should get data.

When executing a `GET` at the root of the controller you get all the elements as a list.
When doing the same at `/controllername/id` you get the element with a specific id.

A `POST` request should generaly add data.

You can only post at the root beceause you cant choose the id of the new element yourself. You could override allready existing elements that way. When posting to an id you should get an error.

To override some stuff, use a `PUT` request.

With a `PUT` to the root you should pass a list (batch)of elements you want to update. Or update a specifiek element by putting at a specific id.

The you can also use the `DELETE` verb which will delete a specific element.

You can't use `DELETE` at the root cause you can't delete the whole collection, instead use `DELETE` at a specific id.

| Resource | Get | Post | Put | Delete |
| :- | :- | :- | :- | :- |
| `/customers` | Get List | Create Item | Update Batch | Error |
| `/customers/123` | Get Item | Error | Update Item | Delete Item |

```cs 
[Route("api/[controller]")]
[ApiController]
public class CustomerController : Controller
{
    //GET: api/Customers
    [HttpGet]
    public ActionResult<IEnumberable<CustomerDTO>> getCustomers()
    {
        // ...
    }

    //GET: api/Customers/4
    [HttpGet("{id}", Name = "GetCustomer")]
    public IActionResult Get(int id)
    {
        try
        {
            Customer customer = db.Customer.FirstOrDefault(c => c.Id == id);
            if (customer == null)
            {
                return NotFound($"Customer with id {id} is not found...");
            }
            return Ok(customer);
        }
        catch (Exception e)
        {
            return BadRequest($"O ok, something ({e.Message}) went wrong");
        }
    }

    //POST: api/Customers
    [HttpPost("")]
    public IActionResult Post([FromBody]Customer customer)
    {
        try
        {
            if (customer != null)
            {
                Customer newCustomer = new Customer()
                {
                    Firstname = customer.FirstName,
                    Lastname = customer.LastName,
                    City = customer.City,
                    Country = customer.Country
                };
                db.Customer.Add(newCumstomer);
                db.saveChanges();
                return Created(Url.Link("GetCustomer", new {id = newCustomer.Id}), newCustomer);
            } else 
            {
                // ...
            }
        }
    }


    //PUT: api/Customers/4
    [Http("{id}")]
    public IActionResult Put(int id, [FromBody]Customer customer)
    {
        try
        {
            Customer oldCustomer = db.Customer.FirstOrDefault(c => c.Id == id);
            if (oldCustomer != null)
            {
                oldCustomer.FirstName = customer.FirstName ?? oldCustomer.FirstName;
                oldCustomer.Lastname = customer.LastName ?? oldCustomer.LastName;
                ...

                db.Customer.Update(oldCustomer);
                db.SaveChanges();

                return Ok(oldCustomer);
            } else 
            {
                // ...
            }
        }
    }


    //DELETE: api/Customers/4
    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        try
         {
             Customer oldCustomer = db.Customer.FirstOrDefault(c => c.Id == id);
             if (oldCustomer != null)
             {
                 db.Customer.Remove(oldCustomer);
                 db.SaveChanges();

                 return OK($"Customer with id {id} has been succesfully deleted");
             } else {
                 return NotFound($"No Customer found with id {id}")
             }
         }
    }
}
```

## generieke maken van de Controller

### generieke Controller
```cs
 public class ControllerCrudBase<T, R> : ControllerBase
        where T : EntityBase // zie onderaan
        where R : Repository<T> // zie onderaan
    {
        protected R repository;

        public ControllerCrudBase(R r)
        {
            repository = r;
        }

        // GET: api/T
        [HttpGet]
        public virtual async Task<IActionResult> Get()
        {
            return Ok(await repository.ListAll());
        }

        // GET: api/T/2
        [HttpGet("{id}")]
        public virtual async Task<IActionResult> Get(int id)
        {
            return Ok(await repository.GetById(id));
        }

        // PUT: api/T/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] T entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entity.Id)
            {
                return BadRequest();
            }

            T e = await repository.Update(entity);
            if (e == null)
            {
                return NotFound();
            }

            return Ok(e);
        }

        // POST: api/T
        [HttpPost]
        public async Task<IActionResult> PostPublisher([FromBody] T entity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            T e = await repository.Add(entity);
            if (e == null)
            {
                return NotFound();
            }

            return CreatedAtAction("Get", new { id = entity.Id }, entity);
        }

        // DELETE: api/T/3
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = await repository.Delete(id);
            if (entity == null)
            {
                return NotFound();
            }

            return Ok(entity);
        }
    }
}
```

### Controller

Een controller die de generieke controller gebruikt + nog extra methode.

```cs
[Route("api/[controller]")]
    [ApiController]
    public class AuthorsController : ControllerCrudBase<Author, AuthorRepository>
    {
        public AuthorsController(AuthorRepository authorRepository): base(authorRepository)
        {
        }

        // GET: api/Authors/Basic
        [HttpGet]
        [Route("Basic")]
        public async Task<IActionResult> GetAuthorBasic()
        {
            var authors = await repository.ListBasic();
            return Ok(authors);
        }
    }
```

### MODELS
#### Standaard model

een standaard model klasse kan gebruikt worden door een model bv. moet een ID hebben, zowel Author als Book als Publisher heeft een ID.

```cs
public abstract class EntityBase
    {
        public int Id { get; set; }
    }
```

#### Model Author implementeert EntityBase

```cs
public class Author: EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }
```


### REPOSITORIES

#### Interface Repo

```cs
public interface IRepository<T> where T : EntityBase
    {
        Task<T> GetById(int id);
        IQueryable<T> GetAll();
        Task<IEnumerable<T>> ListAll();
        IQueryable<T> GetFiltered(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> ListFiltered(Expression<Func<T, bool>> predicate);
    }
```

#### Standaard Repo

hier worden alle methode gezet die gebruikt worden in alle repo's die dit implemteren. Is er toch door omstandigheden een methode die je niet gebruikt of anders gaat gebruiken moet je deze overschrijven. (virtual)

```cs
 public class Repository<T> : IRepository<T> where T : EntityBase
    {
        protected readonly BookServiceContext db;

        public Repository(BookServiceContext context)
        {
            db = context;
        }

        //virtual => het kan overschreven worden bv door AuthorRepository
        public virtual async Task<T> GetById(int id)
        {
            return await db.Set<T>().FindAsync(id);
        }

        // get an IQueryAble: to manipulate with deferred execution
        public virtual IQueryable<T> GetAll()
        {
            // Entities won't be manipulated directly on this set --> faster with AsNoTracking()
            return db.Set<T>().AsNoTracking();
        }


        public async Task<IEnumerable<T>> ListAll()
        {
            return await GetAll().ToListAsync();
        }

        public virtual IQueryable<T> GetFiltered(Expression<Func<T, bool>> predicate)
        {
            return db.Set<T>()
                   .Where(predicate).AsNoTracking();
        }

        public async virtual Task<IEnumerable<T>> ListFiltered(Expression<Func<T, bool>> predicate)
        {
            return await GetFiltered(predicate).ToListAsync();
        }
    }
```


#### Een repository
neemt alle methodes van Repository over, een eigen methode en override een methode

```cs
public class AuthorRepository: Repository<Author>
    {
        public AuthorRepository(BookServiceContext context): base(context)
        {
        }

        public async Task<List<AuthorBasic>> ListBasic()
        {
            var authors = await db.Authors.Select(a => new AuthorBasic
            {
                Id = a.Id,
                Name = $"{a.LastName} {a.FirstName}"

            }).ToListAsync();
            return authors;
        }

        // met override kan je methode overridden (vergeet geen virtual op de overriddende methode)
         public override async Task<T> GetById(int id)
        {
            // iets anders...
        }
    }
````