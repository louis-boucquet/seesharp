# TagHelpers

TagHelpers let you make small changes to a tag by passing parameters to existing tags or even creating your own tags.

Also see ViewComponents for more advanced tags.

## setup

To make a `TagHelper` you first have to make a folder `/TagHelpers`

Then to use it you have to import it. You can import it globaly by adding a line to `/Views/_ViewImports.cshtml`

```html
@addTagHelper *, SolutionName
```

```md
root
├───bin
├───Controllers
├───Models
├───obj
├───Properties
├───TagHelpers // Add this folder
├───Views
│   ├─── _ViewImports.cshtml // Put your imports in this
│   └─── _ViewStart.cshtml
└───wwwroot
```

## Making a TagHelper

In the `TagHelper` folder add a new item and search for "Tag".

This gives you a template TagHelper:

```cs
namespace TagHelperShit.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    [HtmlTargetElement("tag-name")]
    public class RazorTagHelper : TagHelper
    {
        public override void Process(
            TagHelperContext context,
            TagHelperOutput output
        ) {
            // ...
        }
    }
}
```

```cs
namespace TagHelperShit.TagHelpers
{
    // "tag-name" is the name of the tag
    [HtmlTargetElement("tag-name")]
    public class RazorTagHelper : TagHelper
    {
        // the name of an attribute (optional)
        [HtmlAttributeName("asp-attribute-name")]
        public bool attribute { get; set; }

        // this function updates your tag upon creating
        public override void Process(
            TagHelperContext context,
            TagHelperOutput output
        ) {
            if (attriubute) {
                // ...
            }
        }
    }
}
```

## Things you can do

From now on forth the code snippets wil be from the `Process` function.
For ease of reading we won't wrap this every time we write snippets though.

### Getting and setting content

```cs
string originalContent = (await output.GetChildContentAsync()).GetContent();

string processedContent = process(originalContent)

output.Content.SetContent(processedContent);
```

### Changing tag names

```cs
output.TagName = "div";
```

### Setting attributes

```cs
output.Attributes.SetAttribute("attribute-name", "value");
```

Example:

```cs
output.TagName = "a";

output.Attributes.SetAttribute("href", "https://www.google.com");
```

### (Pre/Post)Content and (Pre/Post)Element

Content

```cs
output.PreContent.SetHtmlContent("<strong>");
output.PostContent.SetHtmlContent("</strong>");
```

Element

```cs
output.PreElement.SetHtmlContent("<h1>");
output.PostElement.SetHtmlContent("<h1/>");
```
