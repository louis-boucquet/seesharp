# Vue

Vue works with a root element, everthing starts from the root element. the root element has a id, you also have to just the same id in new Vue (el), see below.


## Vue First App

Paste this link at the foot of your page.
```html
@section scripts{
<script src="~/js/vue.js"></script>
}
```

In IndexView.cshtml (basic)
```html
<article id="app" class="row">
    <header class="col-md-4">
        <h3>{{ message }}</h3> @*Vue mustache*@
    </header>
</article>

@section scripts{
    <script src="~/js/vue.js"></script>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Loading books...',
            }
        });
    </script>
}
```

```html
<article id="app" class="row">
    <header class="col-md-4">
        <h3>{{ message }}</h3> @*Vue mustache*@
        <nav>
            <ul class="list-group">
                <li class="list-group-item" v-for="book in books">
                <span class="bookTitle">{{ book.title }}</span>
                </li>
            </ul>
        </nav>
    </header>
</article>

@section scripts{
    <script src="~/js/vue.js"></script>
    <script>
        var apiURL = 'https://localhost:44396/api/books/basic';
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Loading books...',
                books: null,
            },
            created: function () {
                var self = this;
                self.fetchBooks();
            },
            methods: {
                fetchBooks: function () {
                    // save the "this" context in a variable
                    self = this;
                    fetch(`${apiURL}`)
                    .then(res => res.json())
                    // a function has it's own "this" context
                    .then(function (books) {
                        // because we saved the parent this we can stil access it
                        // another way of doing this is to use an arrow function () => { } which doesn't have a "this" context
                        self.books = books;
                        self.message = 'Overview';
                    })
                    .catch(err => console.error('Error: ' + err));
                },
            }
        });
    </script>
}
```