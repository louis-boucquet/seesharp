# Controler methods

#YEET
Controllers have Action methods as you can see here below.
These get triggerd by `HTTP` requests.

```cs
// basic controller method
public IActionResult Index()
{
    return View("Index");
}
```

Here we return the Index `View`. However, this is the default so this works too (and is the convention).

```cs
// basic controller method
public IActionResult Index()
{
    return View();
}
```

## Parameters

```cs
public IActionResult AddPerson(int age, string name)
{
    // code to process ...
    return View();
}
```

## GET and POST

`GET` http request

```cs
[HttpGet]
public IActionResult AddPerson()
{
    // Show form to make a new Person
    return View();
}
```

`POST` http request

```cs
[HttpPost]
public IActionResult AddPerson(Person person)
{
    return View(person);
}
```

`POST` http request (Extra Options)

```cs
[HttpPost]
public IActionResult AddPerson(Person person)
{
    if (!ModelState.IsValid)
    {
        ModelState.AddModelError(string.Empty, "person cannot be added");

        // return a custom View for failed opperations
        return View("Error", person);
    }
    ModelState.AddModelError("PersonError", "person is added");
    return View(person);
}
```

Here we return the view `Error` or `AddPerson`

## ViewBag and ViewData

The `ViewBag` is a container of variables which you van use in your `View()`.  
ViewBag and ViewData are **NOT** case sensitive.  
Preferably use `ViewBag`!

ViewBag:

```cs
public IActionResult GetPerson(string name)
{
    // code to get person ...
    Person person = /* ... */;

    // This puts Variables in the ViewBag
    ViewBag.FirstName = person.FirstName;
    ViewBag.LastName = person.LastName;
    ViewBag.Age = person.Age;

    return View();
}
```

ViewData:

```cs
public IActionResult GetPerson(string name)
{
    // code to get person ...
    Person person = /* ... */;

    // This puts Variables in the ViewBag
    ViewData["FirstName"] = person.FirstName;
    ViewData["LastName"] = person.LastName;
    ViewData["Age"] = person.Age;

    return View();
}
```

To see how to access thes `ViewBag` See RazorSyntax section

## Model View

You can also use a model `View` which uses 1 Object.

This is prefered over `ViewBag` cause you have to do less manual mapping (from fields to `ViewBag` variables) and it's statically typed.

```cs
public IActionResult GetPerson(int age, string name)
{
    // code to get person ...
    Person person = /* ... */;

    return View(person);
}
```

### Validation

You can validate the fields in the model class.

```cs
public class Person
{
    [Required]
    [Range(typeof(int), "0", "160")]
    public int Age {get; set; }

    [Required]
    public string FirstName {get; set; }

    [Required]
    public string LastName {get; set; }
}
```

more validation options:

* Required
* Range
* Compare
* RegularExpression
* MinLength, MaxLength, StringLength
* Url, Phone, EmailAddress
* Display
* DataType

For more information, check out the [documentation](https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/validation?view=aspnetcore-2.2)

## Redirect

```cs
public IActionResult Index()
{
    // Redirects to the ActionMethod "List"
    return RedirectToAction("List");
}

public IActionResult List()
{
    return View();
}
```
