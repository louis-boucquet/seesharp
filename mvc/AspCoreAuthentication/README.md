# Asp Core authentication

## Authentication vs Authorization

> **Authentication:** Is proving your identity (register and sign in).

> **Authorization:** What resources can you read/write?

### Authentication

#### How to start?

1. Create a new project and click "Change Authentication".
2. Mark "Individual User Accounts" and "Store user accounts in-app".
3. Where are the database tables created? (appsettings.json)
4. Create the database tables 2 options: 
    - Update-Database (NuGet Package Manager Console)
    - Run the application and try to register. Click on "Apply Migrations".

#### Registration requirements

```cs
public void ConfigureServices(IServiceColletion service)
{
    // ...

    services.Configure<IdentityOptions>(options =>
    {
        // Password settings
        options.Password.RequireDigit = true;
        options.Password.RequiredLength = 8;
        // ...

        //Lockout settings
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
        // ...

        // User settings
        options.User.RequireUniqueEmail = true;
    })
    // ...
}
```

### Authorization

- Defining what the user is allowed to do.

#### Preparation

what do we need to secure?

1. Create a table in newly created database. vb Note
2. Execute Scaffold-DBContext (DON'T OVERRIDE DefaultDbContext)
    > `Scaffold-DbContext -Connection "Data Source=.;Initial Catalog=HowestIdentityDemo;Integrated Security=True;" -Provider "Microsoft.EntityFrameworkCore.SqlServer" -OutputDir Data - Contect NoteDbContext -Tables "Note"`
3. ApplicationDbContext, method "OnModelCreating":

```cs
protected override void OnModelCreating(ModelBuilder builder)
{
    base.OnModelCreating(builder);

    builder.Entity<Note>(entity => {
        entity.Property(e => e.NoteId).HasColumnName("NoteID");
        entity.Property(e => e.BodyText).IsRequired().IsUnicode(false);
        entity.Property(e => e.Title).IsRequired().HasMaxLength(50).IsUnicode(false);
    })
}
```

4. Generate a controller with views for CRUD
- Authorize (only allow authorized users)
- AllowAnonymous (Everybody allowed) => higher priority!

bv. Controller met AllowAnoymous en Authorized methode => Authorized zal genegeerd worden.

```cs
[Authorize]
// Only authorized users can visit the NotesController
public class NotesController : Controller
{
    private readonly ApplicationDbContext _context;
    
    public NotesController(ApplicationDbContect context)
    {
        _context = context;
    }
}
```

```cs
[HttpPost]
[ValidateAntiForgeryToken]
[Authorize]
// Only authorized users can consume the Create methode
public async Task<IActionResult> Create([Bind("noteId, Title, BodyText, UserId")] Note note)
{
    if (ModelState.IsValid)
    {
        _context.Add(note);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }
    return View(note);
}
```

#### Roles-based Authorization
A user can belong to multiple roles

Create a methode CreateRoles (Startup.cs)
```cs
private async Task CreateRoles(IServiceProvider serviceProvider)
{
    var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
    var UserManager = serviceProvider.GetReuiredService<UserManager<IdentityUser>>();
    string[] roleNames = {"Admin", "Manager", "Member"};
    IdentityResult roleResult;

    foreach (var roleName in roleNames)
    {
        var roleExist = await RoleManager.RoleExistsAsync(roleName);
        if (!roleExist)
        {
            roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
        }
    }
}
```

Link CreateRoles (Startup.cs)
```cs
public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider) 
// add a IServiceProvider serviceProvider to funtion parameter
        {
            // ...

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            CreateRoles(serviceProvider).Wait();
         
        }

        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            // ...
        }
    }

```

also a connection between API and MVC (Timon)
```cs
[Route("")]
[Route("[controller]")]
public class EventsController : Controller
{
    private HttpClient _client;

    public EventsController()
    {
        _client = new HttpClient()
        {
            BaseAddress = new Uri("http://localhost:2018)
        }
    }

    [Route("")]
    public IActionResult List()
    {
        string eventsResult = _client.GetStringAsync("api/Events").Result;
        List<Event> events = JsonConvert.DeserializeObject<List<Event>>(eventsResult);
        return View(events);
    }

    [Route("[action]/{id}")]
    public IActionResult View(id)
    {
        String eventsResult = _client.GetStringAsync("api/Events/{id}").Result;
        // ...
    }

}

```

Enable identity in the configure services method.
```cs
services.AddIdentity<IdentityUser, IdentityRole>()
    .AddDefaultUI()
    .AddDefaultTokenProviders()
    .AddEntityFrameworkStores<ApplicationDbContext>();
```

Make modifations in the table "AspNetUserRoles"


Make methodes only available for some roles
```cs
//GET: Notes/Create
[Authorize(Roles = "Adimin, SuperUser")]
public IActionResult Create()
{
    return View();
}

// POST: Notes/Create
[HttpPost]
[ValidateAntiForgeryToken]
[Authorize(Roles= "Admin, SuperUser")]
public async Task<IActionResult> Create(....)
```

Limit acces to the views
```cs
<h2>Index</h2>
@if(User.IsInRole("Admin"))
{
    <p>
        <a asp-action="Create">Create New</a>
    </p>
}
```