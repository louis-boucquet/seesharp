# LINQ

These are the basic operators

* First()
* Last()
* ElementAt()
* Skip()
* Take()
* Reverse()
* Count()
* Min()
* Contains()
* Any()
* Concat()
* Union()

## Fancy shit

Zip:

```cs
int[] array1 = {1, 2, 3};
int[] array2 = {4, 5, 6};

array1
    .Zip(array2, (i, j) => i + j);
// 5, 7, 9
```

Select:

```cs
int[] array1 = {1, 2, 3};

array1
    .Select(i => i + 1);
// 2, 3, 4
```

Where:

```cs
int[] array1 = {1, 2, 3};

array1
    .Where(i => i > 1);
// 2, 3
```
