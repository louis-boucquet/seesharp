# MVC

> [ControllerMethods](./ControllerMethods/README.md)  
> [RazorSyntax](./RazorSyntax/README.md)

> [LINQ](./LINQ/README.md)  
> [EntityFramework](./EntityFramework/README.md)

> [SessionStorage](./SessionStorage/README.md)

> [Routing](./Routing/README.md)

> [RestAPI](./RestAPI/README.md)

> [TagHelpers](./TagHelpers/README.md)  
> [ViewComponents (Not course material)](./ViewComponents/README.md)

> [CSRF](./CSRF/README.md)  
> [AspCoreAuthentication](./AspCoreAuthentication/README.md)

> [Vue(SSE)](./Vue/README.md)
