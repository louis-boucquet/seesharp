# Session Storage

## Setup

To enable Session storage change these things in your `Startup.cs` file

```cs
public void ConfigureServices(IServiceCollection services)
{
    /* ... */

    services.AddDistributedMemoryCache();
    services.AddSession(opt => opt.Cookie.IsEssential = true);

    /* ... */
}
```

```cs
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
    /* ... */

    app.UseSession();

    /* ... */
}
```

## Serializing Objects

```cs
SomeObject someObject = /* ... */;

// Serializing
string serializedObject = JsonConvert.SerializeObject(someObject);

// Desirializing
SomeObject deserializedObject =
    JsonConvert.DeserializeObject<SomeObject>(serializedObject);
```

## Accessing from action methods

Reading and writing

```cs
// write data
HttpContext.Session.SetString("bands", data);

// get your data back
HttpContext.Session.GetString("bands")
```

### Example

```cs
private List<Band> GetBands()
{
    string json = HttpContext.Session.GetString("bands");
    if (!string.IsNullOrEmpty(json))
    {
        return JsonConvert.DeserializeObject<List<Band>>(json);
    }
    return BandInitializer.Get();
}
```

```cs
private void AddBands(Band band)
{
    List<Band> bands = GetBands();
    bands.Add(band);
    string toStore = JsonConvert.SerializeObject(bands);

    HttpContext.Session.SetString("bands", toStore);
}
```
