# Entity Framework

First link your database

Automaticaly generate enity code from a database:

```Powershell
Scaffold-DbContext
    "Server=.\sqlexpress;
    Database=Northwind;
    Trusted_Connection=True;"
Microsoft.EntityFrameworkCore.SqlServer -OutputDir
```

## Usage

The previous command (see above) created a directory with all enitity classes.

The more important class is the NorthwindContext. This is where you will access the content of your database.

```cs
NorthwindContext db = new NorthwindContext();

List<Person> people = db.People.ToList();
```

After changing something in the db, you have to save.

```cs
NorthwindContext db = new NorthwindContext();

db.People.Add(new Person {});
db.SaveChanges();
```

## CRUD

CRUD is an acronym for **C**reate **R**ead **U**pdate **D**elete

```cs
public class CustomersController : Controller
{
    NorthWindContect db = new NorthwindContext();
    ...
    // Read => GET: /Customers/First
    public IActionResult First()
    {
        Customers first = db.Customers.FirstOrDefault();
        return Ok(first);
    }

    public IActionResult List()
    {
        var specialCust = db.Customers
            .where(c => c.Country.Contains("a"))
            .OrderBy(c => c.Country)
            .Select(c => c).Tolist();
        return Ok(specialCust);
    }

    // Create
    public IActionResult Create()
    {
        Customer newCustomer = new Customers();
        {
            CustomerId="How",
            CompanyName = "Howest",
            Address = "Rijselstraat 5",
            ContactName = "Mr. Butterfly",
            ContactTitle = "CEO",
            Country= "Belgium"
        };
        db.Customers.Add(newCustomer);
        db.SaveChanges();

        return RedirectToAction("List");
    }

    // Update
    public IActionResult Update()
    {
        Customers updCustomer = db.Customers.First(c => c.CompanyName == "Howest");
        updCustomer.CompanyName = "Howest, De hogeschool";
        updCustomer.Country = ", W-VL";

        db.Update(updCustomer);
        db.SaveChanges();

        return RedirectToAction("List");
    }

    // Delete
    public IActionResult Delete()
    {
        Customers delCustomer = db.Customers
            .FirstOrDefault(c => c.CompanyName.Contains("Howest"));
        if (delCustomer !=null)
        {
            db.Customers.Remove(delCustomer);
            db.SaveChanges();
        }
        return RedirectToAction("List")
    }
}
```

Read (html)

```html
@using NorthwindMVCCore.Entities
@model Customers

<h2>@Model.CompanyName</h2>
<ul>
    <li>@Model.ContactTitle: @Model.ContactName</li>
    <li>@Model.city, @Model.Country</li>
</ul>
```
