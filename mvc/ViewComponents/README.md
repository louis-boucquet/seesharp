# View Components

## Setup

To make a `ViewComponent` you first have to make a folder `/ViewCompontens`

In the `ViewComponents` folder add a new class.

```cs
namespace TagHelperShit.ViewComponents
{
    public class SomeViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult>
        InvokeAsync(List<string> list)
        {
            return View(list);
        }
    }
}
```

This defines a `ViewComponent` with the name `Some`

Now you need to add the razor HTML for this component. To do this make a folder `/Views/Shared/Components/`. This is where all of the HTML for your components will be.

In this case the HTML should be in `/Views/Shared/Components/Some/` and should have the name `Default.cshtml`. Unless you specify this name yourself.

In this file you can put the html for you component:

```html
@model SomeModel

<!-- Some View Code -->
```

Then you should have a project structure like this

```md
root
├───bin
├───Controllers
├───Models
├───obj
├───Properties
├───ViewComponents // Add this folder
│     └───SomeViewComponent.cs // Code for the component
├───Views
└───wwwroot
```

## Using the ViewModel

```html
@{
}

<vc:some param="SomeVariable"></vc:some>
```
