# Routing

## Intro

Take a look at your `Startup.cs` file in the `Configure` method. By default you'll find something like this

```cs
app.UseMvc(routes =>
    {
        routes.MapRoute(
            name: "default",
            template: "{controller=Home}/{action=Index}/{id?}");
    });
```

This is equvalent to this

```cs
app.UseMvcWithDefaultRoute();
```

To use custom routing you need to disable any default routing, like this

```cs
app.UseMvc(routes => { });
```

## Basic routing

Now we can add our custom routed in the controller classes.

By default controllers get these routes.

```cs
// Controller route
[Route("Some")]
public class SomeController : Controller
{
    // Action method route
    [Route("")]
    [Route("Index")]
    public IActionResult Index()
    {
        return View();
    }
}
```

We can also use templates for this

```cs
// Controller route
[Route("[controller]")]
public class SomeController : Controller
{
    // Action method route
    [Route("")]
    [Route("[action]")]
    public IActionResult Index()
    {
        return View();
    }
}
```

## Mapping routes to variables

We can map parts of a route to a variable

```cs
// Controller route
[Route("[controller]")]
public class SomeController : Controller
{
    // Action method route
    [Route("")]
    [Route("[action]/{variable}")]
    public IActionResult Index(string variable)
    {
        return View();
    }
}
```

A variable part of a route can be optional bij adding a `'?'`

note: the variable of the function has to be nullable!

```cs
// Controller route
[Route("[controller]")]
public class SomeController : Controller
{
    // Action method route
    [Route("")]
    [Route("[action]/{variable?}")]
    public IActionResult Index(int? variable)
    {
        return View();
    }
}
```

You can add constraints to variables

```cs
// Controller route
[Route("[controller]")]
public class SomeController : Controller
{
    // Action method route
    [Route("")]
    [Route("[action]/{variable:int:min(8)}")]
    public IActionResult Index(int variable)
    {
        return View();
    }
}
```
