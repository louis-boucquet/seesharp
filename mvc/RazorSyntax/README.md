# Razor Syntax

The file name reflects the action method name.  
(vb. GetPerson.cshtml)

```html
@{
    ViewData["Title"] = "GetPerson";
}

<h2>@ViewData["Title"]</h2>
<p>@ViewBag.FirstName</p>     OF      <p>@ViewData["FirstName"]</p>
<p>@ViewBag.LastName</p>
<p>@ViewBag.Age</p>
```

## TagHelpers

C#

```cs
using System.ComponentModel.DataAnnotations;

namespace FormsTagHelper.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
```

Html

```html
@model FormsTagHelper.ViewModels.RegisterViewModel

<form asp-controller="Demo" asp-action="RegisterInput" method="post">
    Email:  <input asp-for="Email" /> <br />
    Password: <input asp-for="Password" /><br />
    <button type="submit">Register</button>
</form>
```

## Layout

You can add a layout to a view.

```html
<!-- this gets executed first -->
@{
    Layout = "_Layout";
}
```

In a layout somewhere `@RenderBody()` gets called. That is where the html of your view is injected.

## Sections

In a layout page you can call `@RenderSection("name", required: false)`.

to fill this in in a view do this:

```html
@section name {
    <h1>Name!</h1>
}
```
