## Generics

Don't repeat yourself.

```cs
public class MyArray<T>
{
    private T[] array;
    private int arrayIndex = 0;

    public MyArray(int size)
    {
        array = new T[size];
    }

    public void Add(T value)
    {
        array[arrayIndex++] = value;
    }
}
```

How to use:

```cs
MyArray<int> intArray = new MyArray<int>(2);
intArray.Add(1);
intArray.Add(2);

MyArray<string> strArray = new MyArray<string>(2);
strArray.Add("sinaasappel");
strArray.Add("banaan");
```
