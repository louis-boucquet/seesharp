# Yield

With yield you don't have to make an instance of a list yourself.

> **Note:** The return type has to be op type `IEnumerable<T>`

**Without Yield**

```cs
public static IEnumerable<int> Power()
{
    List<int> list = new List<int>();
    
    list.Add(1);
    list.Add(2);
    list.Add(3);

    return list;
}
```

**With Yield**

```cs
public static IEnumerable<int> Power()
{
        yield return 1;
        yield return 2;
        yield return 3;
}
```
