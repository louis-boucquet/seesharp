# switch

**Standaard**

```cs
switch (gender)
{
    case "male":
        Console.WriteLine("Yow dude!");
        break;
    case "female":
        Console.WriteLine("Wadup lady!");
        break;
    default:
        Console.WriteLine("You're weird!");
        break;
}
```

**ranges**

```cs
switch (age)
{
    case int startAge when startAge >= 20 && startAge < 30:
        Console.WriteLine("You're good to go!");
        break;
    case int middleAge when middleAge >= 30 && middleAge < 60:
        Console.WriteLine("...");
        break;
    case int almostOldEnough when (new List<int>() {60,61,62,63}):
        Console.WriteLine("almost");
        break;
}
```

**Types**

```cs
switch (age)
{
    case Array array:
        // ...
        break;
    case IEnumerable<int> ListOfInts:
        // ...
        break;
    case IList list:
        // ...
        break;
}

```
