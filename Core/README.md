# Some interesting core .NET features

Here's some interesting core C# stuff.

> **Note:** this is only course material for SSE, still interesting though.

* [Yield](./Yield/README.md)
* [Switch](./Switch/README.md)
* [Tuples](./Tuples/README.md)
* [Extension Methods](./ExtensionMethods/README.md)
* [Generics](./Generics/README.md)
* [Delegates and Lambda expressions](./Delegates/README.md)
