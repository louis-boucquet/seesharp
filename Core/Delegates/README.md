# Delegates and Lambda expressions

A `delegate` is a type that behaves like a function.
This type can be implemented with a lambda expression.

Let me show you.

**define a `delegate` type**

```cs
namespace Namespace
{
    delegate int NumberParser(string s);
}
```

**implement the `delegate` type**

```cs
// with a lambda expression
NumberParser numberParser = s => int.Parse(s);
// or with a method reference
NumberParser numberParser = int.Parse;
```

**use the `delegate` type**

```cs
string stringToParse;
int parsedNumber = numberParser(stringToParse);
```