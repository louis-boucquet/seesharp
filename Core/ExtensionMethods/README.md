# Extension Methods (this)

adding methodes to existing classes
- without creating a sub class
- without adjusting the original class
- without recompiling the original class library

```cs
static class MyExtensions
{
    public static bool IsValidEmailAddress(this string s)
    {
        Regex regex = new Regex(@^\...);
        return regex.IsMatch(s)
    }
}
```

> **Note:** the `MyExtensions` class has to be `static`

> **Note:** the extension method (`IsValidEmailAddress`) has to be `static`

> **Note:** the extension method (`IsValidEmailAddress`) has to start with a parameter with `this` in front of it

how to use (IsValidEmailAddress)

```cs
string email;
bool isValid = email.IsValidEmailAddress();
```