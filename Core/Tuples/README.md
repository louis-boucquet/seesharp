# Tuples

A tuple is a data structure that contains a sequence of elements of different data types.

**Making a nameless tuple**

```cs
Tuple<string, int, int> tuple = new Tuple<string, int, int>("20-30", 25, 123);
```

**Accessing a nameless tuple**

```cs
tuple.Item1;
tuple.Item2;
tuple.Item3;
```

**Deconstructuring a nameless tuple**

```cs
(string ageRange, int averageAge, int amount) = tuple;
```

**Implicit making of a tuple (like object)**

```cs
(string, string, int) anton = ("Anton", "Plancke", 19)
```

**Implicit making of a named tuple (like object)**

```cs
(string, string, int) anton = (FirstName: "Anton", LastName: "Plancke", Age: 19)
```

**Accessing a named tuple**

```cs
anton.FirstName;
anton.LastName;
anton.Age;
```

**Named tuples from methods**

```cs
// the parameter names map the names for the returned tuple
private static (string FirstName, string LastName, int Age) Method()
{
    return ("Anton",  "Plancke", 19);
}

(string, string, int) tuple = Method();

// Accessing the tuple's data
anton.FirstName;
anton.LastName;
anton.Age;
```
